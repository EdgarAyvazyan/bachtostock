package com.example.domain;

import com.example.enums.UserPriority;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.ArrayList;
import java.util.List;

public class User implements Comparable<User>{
    private String name;
    private boolean premium;
    private int age;
    private List<Product> waitingProductList = new ArrayList<>();
    private UserPriority priority;

    public User(String name, boolean premium, int age) {
        this.name = name;
        this.premium = premium;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isPremium() {
        return premium;
    }

    public void setPremium(boolean premium) {
        this.premium = premium;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public List<Product> getWaitingProductList() {
        return waitingProductList;
    }

    public void setWaitingProductList(List<Product> waitingProductList) {
        this.waitingProductList = waitingProductList;
    }

    public UserPriority getPriority() {
        return priority;
    }

    public void setPriority(UserPriority priority) {
        this.priority = priority;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", premium=" + premium +
                ", age=" + age +
                ", waitingProductList=" + waitingProductList +
                ", priority=" + priority +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return new EqualsBuilder().append(premium, user.premium).append(age, user.age).append(name, user.name).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(name).append(premium).append(age).toHashCode();
    }

    @Override
    public int compareTo(User o) {
        return this.priority.getCode() - o.priority.getCode();
    }
}
