package com.example.enums;

import java.util.HashMap;
import java.util.Map;

public enum UserPriority {
    HIGH(1, "HIgh"),
    MEDIUM(2, "Medium"),
    LOW(3, "Low");

    private static final Map<Integer, UserPriority> codeMap = new HashMap<>();

    static {
        for (UserPriority s : UserPriority.values()) {
            codeMap.put(s.getCode(), s);
        }
    }

    private final int code;
    private final String name;

    UserPriority(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static UserPriority getById(int id) {
        return codeMap.get(id);
    }
}
