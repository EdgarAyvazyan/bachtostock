package com.example.enums;

import java.util.HashMap;
import java.util.Map;

public enum ProductCategory {

    MEDICAL(0, "MEDICAL"),
    BOOKS(1, "BOOKS"),
    DIGITAL(1, "DIGITAL");

    private static final Map<Integer, ProductCategory> codeMap = new HashMap<>();

    static {
        for (ProductCategory s : ProductCategory.values()) {
            codeMap.put(s.getCode(), s);
        }
    }

    private final int code;
    private final String name;

    ProductCategory(int code, String name) {
        this.code = code;
        this.name = name;
    }

    public int getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    public static ProductCategory getById(int id) {
        return codeMap.get(id);
    }

}
