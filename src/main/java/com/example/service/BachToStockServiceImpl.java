package com.example.service;

import com.example.domain.Product;
import com.example.enums.ProductCategory;
import com.example.domain.User;
import com.example.enums.UserPriority;
import com.example.exceptions.UserCreationException;
import com.example.exceptions.UsersUndefinedException;

import java.util.*;
import java.util.logging.Logger;

public class BachToStockServiceImpl implements BackToStockService {
    private final static Logger logger = Logger.getLogger(BachToStockServiceImpl.class.getName());
/**
 * Add product to user and add user in product. Create many-to-many relation
 * @param user
 * @param product */

    @Override
    public void subscribe(User user, Product product) {
        user.getWaitingProductList().add(product);
        product.getUsers().add(user);
    }

/**
 * Searching users by product.
 * Retrieving User that has the product.
 * @param product
 * @return List<User></>*/
    @Override
    public List<User> subscribedUsers(Product product) {

        List<User> users = new ArrayList<>(getAllUsers());
        userPriorityDecision(users);
        Collections.sort(users);
        List<User> resultUsers = new ArrayList<>();
        for (User user : users) {
            if (user.getWaitingProductList().contains(product)) {
                resultUsers.add(user);
                logger.info(user.toString());
            }
        }
        System.out.println("---------------------------");
        return resultUsers;
    }

/**
 * Sorting products by category
 * @param user */
    public void sortProductsByCategory(User user) {
        List<Product> waitingProductList = user.getWaitingProductList();
        Comparator<Product> comparator = Comparator.comparingInt(o -> o.getCategory().getCode());
        waitingProductList.sort(comparator);
    }
/**
 * Retrieving all users
 * @return List<User></>*/
    public List<User> getAllUsers() {
        List<User> users = null;
        try {
            users = initData();
        } catch (UsersUndefinedException e) {
            logger.info(e.getMessage());
        }
        return users;
    }
/**
 * Create User function
 *@param name
 * @param isPrime
 * @param age
 * @return User*/
    public User createUser(String name, boolean isPrime, int age) throws UserCreationException {
        User user = new User(name, isPrime, age);
        if (user == null) {
            logger.info("Cannot create user by name:" + name);
            throw new UserCreationException("Cannot create user by name:" + name);
        }
        return user;
    }

/**
 * Make decision for user priority. HIGH, MEDIUM or LOW
 * @param users*/
    public static void userPriorityDecision(List<User> users) {
        for (User user : users) {
            if (user.isPremium()) {
                user.setPriority(UserPriority.HIGH);
            } else if (user.getAge() > 70) {
                List<Product> waitingProductList = user.getWaitingProductList();
                for (Product product : waitingProductList) {
                    if (product.getCategory().equals(ProductCategory.MEDICAL)) {
                        user.setPriority(UserPriority.HIGH);
                    } else {
                        user.setPriority(UserPriority.MEDIUM);
                    }
                }
            } else {
                user.setPriority(UserPriority.LOW);
            }
        }
    }
/**
 * Initializing Users, Products,
 * sorting user products list
 * @return List<User></>
 * @throws UsersUndefinedException*/
    public List<User> initData() throws UsersUndefinedException {
        List<User> users = new ArrayList<>();
        try {

            Product medical = new Product("01", ProductCategory.MEDICAL);
            Product digital = new Product("02", ProductCategory.DIGITAL);
            Product books = new Product("03", ProductCategory.BOOKS);

            User john = createUser("John", true, 75);
            subscribe(john,medical);
            subscribe(john,digital);
            sortProductsByCategory(john);
            User ann = createUser("Ann", true, 26);
            subscribe(ann,books);
            subscribe(ann,digital);
            sortProductsByCategory(ann);
            User bob = createUser("Bob", false, 35);
            subscribe(bob,medical);
            subscribe(bob,digital);
            subscribe(bob,books);
            sortProductsByCategory(bob);
            User lili = createUser("Lili", false, 81);
            subscribe(lili,medical);
            subscribe(lili,books);
            sortProductsByCategory(lili);
            User mike = createUser("Mike", true, 18);
            subscribe(mike,digital);
            sortProductsByCategory(mike);
            User arnold = createUser("Arnold", false, 25);
            subscribe(arnold, books);
            subscribe(arnold, digital);
            sortProductsByCategory(arnold);
            User chris = createUser("Chris", true, 45);
            subscribe(chris,books);
            sortProductsByCategory(chris);
            User edgar = createUser("Edgar", true, 80);
            subscribe(edgar,medical);
            subscribe(edgar,digital);
            subscribe(edgar,books);
            sortProductsByCategory(edgar);
            User nelli = createUser("Nelli", true, 24);
            subscribe(nelli,digital);
            sortProductsByCategory(nelli);
            User steve = createUser("Steve", false, 29);
            subscribe(steve, digital);
            subscribe(steve, books);
            sortProductsByCategory(steve);

            users.add(john);
            users.add(ann);
            users.add(bob);
            users.add(lili);
            users.add(mike);
            users.add(arnold);
            users.add(chris);
            users.add(edgar);
            users.add(nelli);
            users.add(steve);
        }catch (UserCreationException e) {
            logger.info("No data. Cannot retrieve users");
        }
        if (users.isEmpty()) {
            throw new UsersUndefinedException("Cannot retrieve Users");
        }
        return users;
    }
}
