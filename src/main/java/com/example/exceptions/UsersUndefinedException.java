package com.example.exceptions;

public class UsersUndefinedException extends Exception{
    public UsersUndefinedException(String message) {
        super(message);
    }
}
