package com.example.service;

import com.example.domain.Product;
import com.example.domain.User;
import com.example.enums.ProductCategory;
import com.example.enums.UserPriority;
import com.example.exceptions.UserCreationException;
import com.example.exceptions.UsersUndefinedException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class BackToStockServiceImplTest {

    @Test
    public void subscribeUserTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User user = new User("Rafayel", true, 29);
        Product product = new Product("01", ProductCategory.MEDICAL);

        bachToStockService.subscribe(user, product);
        assertEquals(user.getWaitingProductList().get(0), product);
    }

    @Test
    public void subscribeProductTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User user = new User("Rafayel", true, 29);
        Product product = new Product("01", ProductCategory.MEDICAL);

        bachToStockService.subscribe(user, product);
        assertEquals(product.getUsers().get(0), user);
    }

    @Test
    public void subscribeUserNegativeTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User user = new User("Rafayel", true, 29);
        Product product = new Product("01", ProductCategory.MEDICAL);
        Product product1 = new Product("02", ProductCategory.DIGITAL);

        bachToStockService.subscribe(user, product);
        Assertions.assertNotEquals(user.getWaitingProductList().get(0), product1);
    }

    @Test
    public void subscribeProductNegativeTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User user = new User("Rafayel", true, 29);
        User user1 = new User("Bob", false, 66);
        Product product = new Product("01", ProductCategory.MEDICAL);

        bachToStockService.subscribe(user, product);
        Assertions.assertNotEquals(product.getUsers().get(0), user1);
    }

    @Test
    public void createUserTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User jain = null;
        try {
            jain = bachToStockService.createUser("Jain", true, 45);
        } catch (UserCreationException e) {
            e.printStackTrace();
        }
        assertNotNull(jain);
        assertTrue(jain instanceof User);
    }

    @Test
    public void getAllUsersTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        List<User> allUsers = bachToStockService.getAllUsers();
        assertNotNull(allUsers);
        assertFalse(allUsers.isEmpty());
        assertNotNull(allUsers.get(0));
        assertEquals(allUsers.get(0).getClass(), User.class);
    }

    @Test
    public void sortProductsByCategoryTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        User user = new User("John", true, 25);
        Product product1 = new Product("001", ProductCategory.MEDICAL);
        Product product2 = new Product("002", ProductCategory.BOOKS);
        Product product3 = new Product("003", ProductCategory.DIGITAL);

        user.getWaitingProductList().add(product2);
        user.getWaitingProductList().add(product3);
        user.getWaitingProductList().add(product1);

        assertEquals(user.getWaitingProductList().get(0).getCategory(), ProductCategory.BOOKS);
        assertEquals(user.getWaitingProductList().get(1).getCategory(), ProductCategory.DIGITAL);
        assertEquals(user.getWaitingProductList().get(2).getCategory(), ProductCategory.MEDICAL);

        bachToStockService.sortProductsByCategory(user);
        assertEquals(user.getWaitingProductList().get(0).getCategory(), ProductCategory.MEDICAL);
        assertEquals(user.getWaitingProductList().get(1).getCategory(), ProductCategory.BOOKS);
        assertEquals(user.getWaitingProductList().get(2).getCategory(), ProductCategory.DIGITAL);

    }

    @Test
    public void userPriorityDecisionTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();

        User user1 = new User("John", false, 25);//LOW
        User user2 = new User("Ann", false, 72);//MED
        User user3 = new User("Bob", true, 81);//HI
        User user4 = new User("Steve", true, 36);//HI
        Product product1 = new Product("001", ProductCategory.MEDICAL);
        Product product2 = new Product("002", ProductCategory.BOOKS);
        Product product3 = new Product("003", ProductCategory.DIGITAL);

        user1.getWaitingProductList().add(product1);
        user1.getWaitingProductList().add(product3);

        user2.getWaitingProductList().add(product1);
        user2.getWaitingProductList().add(product2);

        user3.getWaitingProductList().add(product1);
        user3.getWaitingProductList().add(product2);
        user3.getWaitingProductList().add(product3);

        user4.getWaitingProductList().add(product3);
        user4.getWaitingProductList().add(product2);

        List<User> users = new ArrayList<>();
        users.add(user1);
        users.add(user2);
        users.add(user3);
        users.add(user4);

        BachToStockServiceImpl.userPriorityDecision(users);
        Collections.sort(users);

        assertEquals(user1.getPriority(), UserPriority.LOW);
        assertEquals(user2.getPriority(), UserPriority.MEDIUM);
        assertEquals(user3.getPriority(), UserPriority.HIGH);
        assertEquals(user4.getPriority(), UserPriority.HIGH);

    }

    @Test
    public void initDataTest() throws UsersUndefinedException {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        List<User> users = bachToStockService.initData();

        assertFalse(users.isEmpty());
        assertEquals(users.get(0).getClass(), User.class);
    }

    @Test
    public void subscribedUsersTest() {
        BachToStockServiceImpl bachToStockService = new BachToStockServiceImpl();
        Product product1 = new Product("01", ProductCategory.MEDICAL);
        Product product2 = new Product("02", ProductCategory.BOOKS);
        Product product3 = new Product("03", ProductCategory.DIGITAL);
        List<User> users1 = bachToStockService.subscribedUsers(product1);
        List<User> users2 = bachToStockService.subscribedUsers(product2);
        List<User> users3 = bachToStockService.subscribedUsers(product3);

        for (User user : users1) {
            assertTrue(user.getWaitingProductList().contains(product1));
            break;
        }

        for (User user : users2) {
            assertTrue(user.getWaitingProductList().contains(product2));
            break;
        }

        for (User user : users3) {
            assertTrue(user.getWaitingProductList().contains(product3));
            break;
        }
    }

}
